package com.example.worldmessage

data class ModelUser(
    var email: String,
    var firstName: String,
    var lastName: String,
    var password: String,
    var id: String,
    var chatList: List<String>
)
