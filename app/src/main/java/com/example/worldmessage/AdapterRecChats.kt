package com.example.worldmessage

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Response

class AdapterRecChats(private val list: MutableList<ModelChat>): RecyclerView.Adapter<AdapterRecChats.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val avatar = itemView.findViewById<ImageView>(R.id.avatar_icon)
        val name_user = itemView.findViewById<TextView>(R.id.name_user)
        val last_message = itemView.findViewById<TextView>(R.id.last_message)
        val time = itemView.findViewById<TextView>(R.id.time_lost)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_chat, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = list[position]
        holder.last_message.text = model.lastMessage.message
        var requst_id = ""
        if (model.idUser1 == Info.user_id){
            requst_id = model.idUser2
        }else{
            requst_id = model.idUser1
        }
        holder.name_user.text = "Загрузка..."
        initRetrofit().create(Api::class.java).getUser(requst_id).enqueue(object: retrofit2.Callback<ModelUser>{
            override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
                holder.name_user.text = "${response.body()!!.firstName} ${response.body()!!.lastName}"
            }

            override fun onFailure(call: Call<ModelUser>, t: Throwable) {
                showError(t, holder.itemView.context)
            }
        })
    }

    override fun getItemCount(): Int = list.size

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}