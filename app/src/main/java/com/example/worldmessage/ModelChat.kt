package com.example.worldmessage

data class ModelChat(
    var idChat: String,
    var idUser1: String,
    var idUser2: String,
    var lastMessage: ModelMessage,
    var messageList: List<ModelMessage>
)
