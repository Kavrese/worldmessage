package com.example.worldmessage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Handler().postDelayed({
          //  startActivity(Intent(this, AuthScreen::class.java))
            startActivity(Intent(this, MainScreen::class.java))
            finish()
        }, 1500)
    }
}