package com.example.worldmessage

data class ModelMessage(
    var dateMessage: String,
    var idUser: String,
    var idMessage: Int,
    var message: String
)
