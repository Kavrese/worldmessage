package com.example.worldmessage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_reg_screen.*

fun reg(first_name: String, last_name: String, email: String, password: String){
    initRetrofit()
}

class RegScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg_screen)

        card_reg.setOnClickListener {
            if (checkEmailAndPassword(
                    email_reg.text.toString(),
                    password1_reg.text.toString(),
                    password2_reg.text.toString(),
                    this
                ) && checkFirstLastNamesEditText(first_name_reg, last_name_reg, this)
            ){
                reg(first_name_reg.text.toString(), last_name_reg.text.toString(), email_reg.text.toString(), password1_reg.text.toString())
            }
        }
    }
}