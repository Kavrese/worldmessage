package com.example.worldmessage

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_auth_screen.*
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory

fun auth(email: String, password: String, context: Context){
    val shortEmail = email.substringBefore("@")
    initRetrofit().create(Api::class.java).getUser(shortEmail).enqueue(object: Callback<ModelUser>{
        override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
            if (response.body()!!.password == password){

                val model = response.body()!!
                Info.user_email = model.email
                Info.user_first_name = model.firstName
                Info.user_last_name = model.lastName
                Info.user_password = model.password
                Info.user_id = shortEmail

                context.startActivity(Intent(context, MainScreen::class.java))
                (context as Activity).finish()
            }else{
                showAlert("Error Body Null", context)
            }
        }

        override fun onFailure(call: Call<ModelUser>, t: Throwable) {
            showError(t, context)
        }
    })
}

fun initRetrofit(): Retrofit{
    return Retrofit.Builder()
        .baseUrl("https://no")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

fun showAlert(mes: String, context: Context){
    Toast.makeText(context, mes, Toast.LENGTH_LONG).show()
}

fun showError(mes: Throwable, context: Context){
    Toast.makeText(context, mes.message, Toast.LENGTH_LONG).show()
}

fun checkEmailAndPassword(email: String, password1: String, password2: String = password1, context: Context): Boolean{
    if("@" !in email){
        showAlert("Введите email корректно", context)
        return false
    }
    if (password1 != password2){
        showAlert("Пароли не совпадают", context)
        return false
    }
    return true
}

fun checkFirstLastNamesEditText(first_name: EditText, last_name: EditText, context: Context): Boolean{
    if (first_name.text.isEmpty() && last_name.text.isEmpty()){
        showAlert("Имя и фамилия должны быть заполненны", context)
        return false
    }
    return true
}

class AuthScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth_screen)
        card_auth.setOnClickListener{
            if (checkEmailAndPassword(email.text.toString(), password.text.toString(), context = this)){
                auth(email.text.toString(), password.text.toString(), this)
            }
        }
    }
}