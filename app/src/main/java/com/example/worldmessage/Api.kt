package com.example.worldmessage

import retrofit2.Call
import retrofit2.http.*

interface Api {
    @GET("https://enterwork-2bdb8.firebaseio.com/Users/{idUser}.json")
    fun getUser(@Path("idUser") idUser: String): Call<ModelUser>

    @GET("https://enterwork-2bdb8.firebaseio.com/Chat/{idChat}.json")
    fun getChat(@Path("idChat") idChat: String): Call<ModelChat>

    @PUT("https://enterwork-2bdb8.firebaseio.com/Users/{idUser}.json")
    fun newUser(@Path("idUser") idUser: String, @Body body: ModelUser): Call<ModelUser>

    @PUT("https://enterwork-2bdb8.firebaseio.com/Chat/{idChat}.json")
    fun newChat(@Path("idChat") idChat: String, @Body body: ModelChat): Call<ModelChat>

    @PUT("https://enterwork-2bdb8.firebaseio.com/Chat/{idChat}/messageList/{idMessage}.json")
    fun newMessage(@Path("idMessage") idMessage: String, @Path("idChat") idChat: String, @Body body: ModelMessage): Call<ModelMessage>

    @PATCH("https://enterwork-2bdb8.firebaseio.com/Chat/{idChat}/messageList/{idMessage}.json")
    fun updateMessage(@Path("idChat") idChat: String, @Path("idMessage") idMessage: String, @Body body: ModelMessage): Call<ModelMessage>

    @DELETE("https://enterwork-2bdb8.firebaseio.com/Users/{idUser}.json")
    fun deleteUser(@Path("idUser") idChat: String)

    @DELETE("https://enterwork-2bdb8.firebaseio.com/Chat/{idChat}.json")
    fun deleteChat(@Path("idChat") idChat: String)
}