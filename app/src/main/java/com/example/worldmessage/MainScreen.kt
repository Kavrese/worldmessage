package com.example.worldmessage

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main_screen.*
import kotlinx.android.synthetic.main.dialog.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainScreen : AppCompatActivity() {

    val listChats: MutableList<ModelChat> = mutableListOf(
            //ModelChat("no", "000001", "000001", ModelMessage("", "", 0, ""), arrayListOf())
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_screen)

        rec_chat.apply {
            adapter = AdapterRecChats(listChats)
            layoutManager = LinearLayoutManager(this@MainScreen)
        }

        add.setOnClickListener {
            val view = View.inflate(this, R.layout.dialog, null)
            val dialog = AlertDialog.Builder(this)
                .setView(view)
                .create()
            view.ok_dialog.setOnClickListener {
                if (view.add_id_user.text.length == 6) {
                    //Тут должно быть добавленние нового чата
                }
                dialog.dismiss()
            }
            dialog.setCanceledOnTouchOutside(true)
            dialog.show()
        }

        initChats()
    }

    private fun initChats() {
       ChatInit(object : UpdateDataAdapter{
           override fun addNewChat(modelChat: ModelChat) {
               listChats.add(modelChat)
               rec_chat.adapter!!.notifyDataSetChanged()
               no_chat_icon.visibility = View.INVISIBLE
           }
       }).execute()

    }

    class ChatInit(val update: UpdateDataAdapter) :
        AsyncTask<Void, Void, Boolean>() {

        override fun doInBackground(vararg params: Void?): Boolean {
            initRetrofit().create(Api::class.java).getUser(Info.user_id!!).enqueue(object: retrofit2.Callback<ModelUser>{
                override fun onResponse(call: Call<ModelUser>, response: Response<ModelUser>) {
                    for (i in response.body()!!.chatList) {
                        initRetrofit().create(Api::class.java).getChat(i).enqueue(object: Callback<ModelChat>{
                            override fun onResponse(call: Call<ModelChat>, response: Response<ModelChat>) {
                                update.addNewChat(response.body()!!)
                            }

                            override fun onFailure(call: Call<ModelChat>, t: Throwable) {

                            }

                        })
                    }
                }

                override fun onFailure(call: Call<ModelUser>, t: Throwable) {

                }

            })
            return true
        }

        override fun onPostExecute(result: Boolean?) {

        }
    }
}